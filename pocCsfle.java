package com.mongodb.qe;

import java.util.*;

import java.util.HashMap;
import java.util.Map;


import org.bson.BsonArray;
import org.bson.BsonBinary;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.BsonInt32;
import org.bson.BsonBoolean;

import com.mongodb.AutoEncryptionSettings;
import com.mongodb.ClientEncryptionSettings;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.vault.DataKeyOptions;
import com.mongodb.client.vault.ClientEncryption;
import com.mongodb.client.vault.ClientEncryptions;

public class PocCsfle {

    public static void main(String[] args) throws Exception {
        String connectionString = "mongodb://localhost:27017/";

        // start-kmsproviders
        Map<String, Map<String, Object>> kmsProviders = new HashMap<>();
        String kmsProvider = "aws";
        Map<String, Object> providerDetails = new HashMap<>();
        providerDetails.put("accessKeyId", new BsonString("<<ACCESS_KEY_ID>>"));
        providerDetails.put("secretAccessKey", new BsonString("<<SECRET_ACCESS_KEY>>"));
        kmsProviders.put(kmsProvider, providerDetails);
        // end-kmsproviders

        // start-datakeyopts
        BsonDocument masterKeyProperties = new BsonDocument();
        masterKeyProperties.put("provider", new BsonString(kmsProvider));
        masterKeyProperties.put("key", new BsonString("<<ARN_VALUE>>"));
        masterKeyProperties.put("region", new BsonString("<<REGION>>"));
        // end-datakeyopts

        // start-create-index
        String keyVaultDb = "encryption";
        String keyVaultColl = "__keyVault";
        String keyVaultNamespace = keyVaultDb + "." + keyVaultColl;
        MongoClient keyVaultClientForIndex = MongoClients.create(connectionString);

        // Drop the Key Vault Collection in case you created this collection
        // in a previous run of this application.
        keyVaultClientForIndex.getDatabase(keyVaultDb).getCollection(keyVaultColl).drop();
        keyVaultClientForIndex.getDatabase("medicalRecords").getCollection("patients").drop();

        MongoCollection keyVaultCollection = keyVaultClientForIndex.getDatabase(keyVaultDb).getCollection(keyVaultColl);
        IndexOptions indexOpts = new IndexOptions().partialFilterExpression(new BsonDocument("keyAltNames", new BsonDocument("$exists", new BsonBoolean(true) ))).unique(true);
        keyVaultCollection.createIndex(new BsonDocument("keyAltNames", new BsonInt32(1)), indexOpts);
        keyVaultClientForIndex.close();
        // end-create-index

        // start-create-dek
        ClientEncryptionSettings clientEncryptionSettings = ClientEncryptionSettings.builder()
                .keyVaultMongoClientSettings(MongoClientSettings.builder()
                        .applyConnectionString(new ConnectionString(connectionString))
                        .build())
                .keyVaultNamespace(keyVaultNamespace)
                .kmsProviders(kmsProviders)
                .build();
        ClientEncryption clientEncryption = ClientEncryptions.create(clientEncryptionSettings);

        List<String> keyAlts1 = new ArrayList<>();
        keyAlts1.add("demo-data-key");
        BsonBinary dataKeyId1 = clientEncryption.createDataKey(kmsProvider, new DataKeyOptions()
                .masterKey(masterKeyProperties)
                .keyAltNames(keyAlts1));
        String base64DataKeyId1 = Base64.getEncoder().encodeToString(dataKeyId1.getData());
        logger.info("datakeyId1 base64: {}", base64DataKeyId1);
        // end-create-dek
//        clientEncryption.close();

        String encryptedDbName = "medicalRecords";
        String encryptedCollName = "patients";

        // start-create-enc-collection
        String encryptedNameSpace = encryptedDbName + "." + encryptedCollName;

        Document jsonShema = new Document().
                append("bsonType", "object")
                .append("mobileNumber", new Document().append("encrypt", new Document()
                        .append("bsonType", "string")
                        .append("algorithm", "AEAD_AES_256_CBC_HMAC_SHA_512-Random")
                        .append("keyId", "/key-id")));

        HashMap<String, BsonDocument> schemaMap = new HashMap<>();
        schemaMap.put(encryptedNameSpace, BsonDocument.parse(jsonSchema.toJson()));
        // end-schema

        // start-extra-options // check for dependency
        Map<String, Object> extraOptions = new HashMap<>();
        extraOptions.put("cryptSharedLibPath", "<<SHARED_LIB_PATH>>");
        extraOptions.put("cryptSharedLibRequired", true);
        // end-extra-options

        // start regular mongo client
        MongoClientSettings clientSettingsRegular = MongoClientSettings.builder()
                .applyConnectionString(new ConnectionString(connectionString))
                .build();

        MongoClient mongoClientRegular = MongoClients.create(clientSettingsRegular);

        // start secure mongo client
        MongoClientSettings secureClientSettings = MongoClientSettings.builder()
                .applyConnectionString(new ConnectionString(connectionString))
                .autoEncryptionSettings(AutoEncryptionSettings.builder()
                        .keyVaultNamespace(keyVaultNamespace)
                        .kmsProviders(kmsProviders)
                        .schemaMap(schemaMap)
                        .extraOptions(extraOptions)
                        .build())
                .build();

        MongoClient mongoClientSecure = MongoClients.create(secureClientSettings);
        logger.info("Successfully connected to secure mongo client!");

        // Insert Data
        // start-insert
        Document customer = new Document()
                .append("mobileNumber", "123456789")
                .append("key-id", "demo-data-key");
        Document customer2 = new Document()
                .append("mobileNumber", "987654321")
                .append("key-id", "demo-data-key");
        // end-insert

        /**
         * Here are few open questions -
         * 1. I want to encrypt only 1 field from object i.e mobileNumber but I am getting error
         * Exception in encryption library: csfle "analyze_query" failed: Unknown $jsonSchema keyword: mobileNumber [Error 2, code 9]
         * Please help here what will be the exact schema for encryption.
         * My object will look like this -
         *  {
         *   "mobileNumber": "123456789"
         *  }
         * 2. Also my object will contains serverl fields as well like address, message etc.
         * Do I need to provide each field detail in schema validation ?
         * so final object will look like this -
         * {
         *     "mobileNumber": "123456789",
         *     "message": "my message",
         *     "address": {
         *          "city": "city"
         *     }
         *     .
         *     .
         *     .
         *
         * }
         * 3. What is significance of "key-id"? As per documentation in mongo, every time I need to give this value.
         * So lets say I want to insert documents 2 times, what should be the structure?
         */

        InsertOneResult insertOneResult = mongoClientSecure.getDatabase(encryptedDbName).getCollection(encryptedCollName).insertOne(customer);
        InsertOneResult insertOneResult1 = mongoClientSecure.getDatabase(encryptedDbName).getCollection(encryptedCollName).insertOne(customer2);
        logger.info("Insert result: {}", insertOneResult);
        logger.info("Insert result: {}", insertOneResult1);

        // start-find
        System.out.println("Finding a document with regular (non-encrypted) client.");
        Document docRegular = mongoClientRegular.getDatabase(encryptedDbName).getCollection(encryptedCollName).find().first();
        logger.info("Docu regular: {}", docRegular.toJson());

        System.out.println("Finding a document with encrypted client, searching on an encrypted field");
        Document docSecure = mongoClientSecure.getDatabase(encryptedDbName).getCollection(encryptedCollName).find().first();
        logger.info("Docu secure: {}", docSecure.toJson());
        // end-find
        mongoClientSecure.close();
        mongoClientRegular.close()
    }
}
